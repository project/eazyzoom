<?php

/**
 * @file
 * Provides an Eazy Zoom field formatter for Image fields.
 */

/**
 * Implements hook_libraries_info().
 */
function easyzoom_libraries_info() {

  $libraries['easyzoom'] = array(
    'name' => 'EasyZoom library',
    'vendor url' => 'http://i-like-robots.github.io/EasyZoom',
    'download url' => 'https://github.com/i-like-robots/EasyZoom/zipball/master',
    'version callback' => 'easyzoom_library_version',
    'files' => array(
      'js' => array('dist/easyzoom.js'),
    ),
  );

  return $libraries;
}

/**
 * Eazyzoom library version callback.
 *
 * @return boolean
 */
function easyzoom_library_version() {
  return TRUE;
}

/**
 * Implements hook_field_formatter_info().
 */
function easyzoom_field_formatter_info() {
  return array(
    'easyzoom_formatter' => array(
      'label' => t('Easy Zoom Image'),
      'field types' => array('image'),
      'settings'  => array(
        'image_style' => '',
        'zoom_image_style' => '',
        'zoom_type' => 'overlay',
        'has_thumbnails' => FALSE,
      ),
    ),
    'easyzoom_thumbnail_formatter' => array(
      'label' => t('Easy Zoom Thumbnail'),
      'field types' => array('image'),
      'settings'  => array(
        'thumbnail_style' => '',
        'image_style' => '',
        'zoom_image_style' => '',
      ),
    ),
    'easyzoom_gallery_formatter' => array(
      'label' => t('Easy Zoom Gallery'),
      'field types' => array('image'),
      'settings'  => array(
        'thumbnail_style' => '',
        'image_style' => '',
        'zoom_image_style' => '',
        'zoom_type' => 'overlay',
        'has_thumbnails' => TRUE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function easyzoom_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  if ($display['type'] == 'easyzoom_thumbnail_formatter' || $display['type'] == 'easyzoom_gallery_formatter') {
    $element['thumbnail_style'] = array(
      '#type'           => 'select',
      '#title'          => t('Thumbnail Image Style'),
      '#default_value'  => $settings['thumbnail_style'],
      '#empty_option' => t('None (original image)'),
      '#options' => image_style_options(FALSE),
    );
  }

  $element['image_style'] = array(
    '#type'           => 'select',
    '#title'          => t('Image Style'),
    '#default_value'  => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => image_style_options(FALSE),
  );

  $element['zoom_image_style'] = array(
    '#type'           => 'select',
    '#title'          => t('Zoom Image Style'),
    '#default_value'  => $settings['zoom_image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => image_style_options(FALSE),
  );

  if ($display['type'] == 'easyzoom_formatter' || $display['type'] == 'easyzoom_gallery_formatter') {
    $element['zoom_type'] = array(
      '#type' => 'select',
      '#title' => t('Zoom Type'),
      '#description' => t('Select what type of zomm effect you would like.'),
      '#default_value' => $settings['zoom_type'],
      '#options' => array(
        'overlay' => 'overlay',
        'adjacent' => 'adjacent',
      ),
    );
  }

  if ($display['type'] == 'easyzoom_formatter') {
    $element['has_thumbnails'] = array(
      '#type' => 'checkbox',
      '#title' => t('Has Thumbnails'),
      '#description' => t('You will need another field with thumbnails'),
      '#default_value'  => $settings['has_thumbnails'],
      '#empty_option' => FALSE,
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function easyzoom_field_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = array();
  $image_styles = image_style_options(FALSE);
  unset($image_styles['']);

  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $style = 'Original image';
  if (isset($image_styles[$settings['image_style']])) {
    $style = $image_styles[$settings['image_style']];
  }

  $zoom_image_style = 'Original image';
  if (isset($image_styles[$settings['zoom_image_style']])) {
    $zoom_image_style = $image_styles[$settings['zoom_image_style']];
  }

  // Summary messages.
  $summary[] = t('Image style: @style', array(
    '@style' => $style,
    )
  );
  $summary[] = t('Zoom image style: @zoom_image_style', array(
    '@zoom_image_style' => $zoom_image_style,
  ));

  if ($display['type'] == 'easyzoom_formatter') {
     $summary[] = t('Zoom type: @zoom_type', array(
      '@zoom_type' => $settings['zoom_type'],
    ));
  }

  if ($display['type'] == 'easyzoom_thumbnail_formatter' || $display['type'] == 'easyzoom_gallery_formatter') {
    $thumbnail_image_style = 'Original image';
    if (isset($image_styles[$settings['thumbnail_style']])) {
      $thumbnail_image_style = $image_styles[$settings['thumbnail_style']];
    }

    $summary[] = t('Thumbnail style: @thumb', array(
      '@thumb' => $thumbnail_image_style,
      )
    );
  }

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function easyzoom_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  foreach ($items as $delta => $item) {
    if ($display['type'] == 'easyzoom_formatter') {
      $zoom_image_url = $settings['zoom_image_style'] ? image_style_url($settings['zoom_image_style'], $item['uri']) : file_create_url($item['uri']);

      $element[$delta] = array(
        '#theme' => 'easyzoom_image',
        '#item' => $item,
        '#image' => easyzoom_get_image_tag($settings['image_style'], $item),
        '#zoom_image_url' => $zoom_image_url,
        '#zoom_type' => $settings['zoom_type'],
        '#has_thumbnails' => $settings['has_thumbnails'],
      );
    }
    elseif ($display['type'] == 'easyzoom_thumbnail_formatter') {
      $image_url = $settings['image_style'] ? image_style_url($settings['image_style'], $item['uri']) : file_create_url($item['uri']);
      $zoom_image_url = $settings['zoom_image_style'] ? image_style_url($settings['zoom_image_style'], $item['uri']) : file_create_url($item['uri']);

      $element[$delta] = array(
        '#theme' => 'easyzoom_thumbnail_image',
        '#item' => $item,
        '#thumbnail' => easyzoom_get_image_tag($settings['thumbnail_style'], $item),
        '#image_url' => $image_url,
        '#zoom_image_url' => $zoom_image_url,
      );
    }

    if ($display['type'] == 'easyzoom_gallery_formatter') {
      // The first image of the gallery will have a large image.
      if ($delta == 0) {
        $element[$delta] = easyzoom_get_image_element($item, $settings);
      }

      $element[$delta + 1] = easyzoom_get_thumbnail_element($item, $settings);
    }
  }

  $easyzoom_library_path = libraries_get_path('easyzoom');

  $element['#attached']['css'] = array(
    $easyzoom_library_path . '/css/easyzoom.css',
  );

  $element['#attached']['js'] = array(
    $easyzoom_library_path . '/src/easyzoom.js' => array('scope' => 'footer'),
    drupal_get_path('module', 'easyzoom') . '/js/easyzoom_script.js' => array('scope' => 'footer'),
  );

  return $element;
}

/**
 * Get image element array.
 *
 * @param  array $item     The element's item array.
 * @param  array $settings The field formatters settings.
 *
 * @return array           The element being rendered.
 */
function easyzoom_get_image_element($item, $settings) {
  $zoom_image_url = $settings['zoom_image_style'] ? image_style_url($settings['zoom_image_style'], $item['uri']) : file_create_url($item['uri']);

  $element = array(
    '#theme' => 'easyzoom_image',
    '#item' => $item,
    '#image' => easyzoom_get_image_tag($settings['image_style'], $item),
    '#zoom_image_url' => $zoom_image_url,
    '#zoom_type' => $settings['zoom_type'],
    '#has_thumbnails' => $settings['has_thumbnails'],
  );

  return $element;
}

/**
 * Get thumbnail element array.
 *
 * @param  array $item     The element's item array.
 * @param  array $settings The field formatters settings.
 *
 * @return array           The element being rendered.
 */
function easyzoom_get_thumbnail_element($item, $settings) {
  $image_url = $settings['image_style'] ? image_style_url($settings['image_style'], $item['uri']) : file_create_url($item['uri']);
  $zoom_image_url = $settings['zoom_image_style'] ? image_style_url($settings['zoom_image_style'], $item['uri']) : file_create_url($item['uri']);

  $element = array(
    '#theme' => 'easyzoom_thumbnail_image',
    '#item' => $item,
    '#thumbnail' => easyzoom_get_image_tag($settings['thumbnail_style'], $item),
    '#image_url' => $image_url,
    '#zoom_image_url' => $zoom_image_url,
  );

  return $element;
}

/**
 * Get image tag.
 *
 * @param  string  $style_name  The image style name.
 * @param  array   $item        The image being processed.
 * @param  array   $attributes  Image html attributes.
 *
 * @return string               Image html.
 */
function easyzoom_get_image_tag($style_name, $item, $attributes = array()) {
  $image = array(
    'path' => $item['uri'],
    'alt' => $item['alt'],
    'style_name' => $style_name,
  );

  if ($attributes) {
    $image['attributes'] = $attributes;
  }

  // Do not output an empty 'title' attribute.
  if (!empty($item['title'])) {
    $image['title'] = $item['title'];
  }

  return !empty($style_name) ? theme('image_style', $image) : theme('image', $image);
}

/**
 * Implements hook_theme().
 */
function easyzoom_theme($existing, $type, $theme, $path) {
  return array(
    'easyzoom_image' => array(
      'variables' => array(
        'item' => NULL,
        'image' => NULL,
        'zoom_image_url' => NULL,
        'zoom_type' => NULL,
        'has_thumbnails' => NULL,
      ),
      'template' => 'easyzoom-image',
      'path' => $path . '/theme',
    ),
    'easyzoom_thumbnail_image' => array(
      'variables' => array(
        'item' => NULL,
        'thumbnail' => NULL,
        'image_url' => NULL,
        'zoom_image_url' => NULL,
      ),
      'template' => 'easyzoom-thumbnail',
      'path' => $path . '/theme',
    ),
  );
}

/**
 * Preprocess function for easyzoom_image.
 */
function template_preprocess_easyzoom_image(&$variables) {
  $classes = array();
  $classes[] = 'easyzoom';

  // Add classes based on type that was chosen.
  $classes[] = 'easyzoom--' . $variables['zoom_type'];

  if ($variables['has_thumbnails']) {
    $classes[] = 'easyzoom--with-thumbnails';
  }

  $variables['class'] = implode($classes, ' ');
}
